package com.gitlab.pietrom.esbank.core

class Hello(val message: String) {
    fun sayHello(to: String) = "${message}, ${to}!"
}