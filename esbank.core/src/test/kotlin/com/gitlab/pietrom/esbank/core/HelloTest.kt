package com.gitlab.pietrom.esbank.core

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class HelloTest {
    @Test
    fun canSayHelloToSomeone() {
        assertThat(Hello("Hello").sayHello("pietrom"), `is`("Hello, pietrom!"))
    }
}